const db = require("../models");
const StockShare = db.stockShares;
const Op = db.sequelize.Op;


exports.create = (req,res) => {
    if(!req.body.inscode){
        res.status(400).send({
            message:"Bad request!!!"
        });
        return;
    }
    const stockShare = {
        name:req.body.name,
        inscode : req.body.inscode,
        value : req.body.value
    };
    StockShare.create(stockShare)
    .then(data => {res.send(data);})
    .catch(err => {
        res.status(500).send({
            message:err.message||"be fana raft"
        });
    });
};

exports.findAll = (req,res) => {
    const inscode = req.query.inscode;
    var condition = inscode ? { inscode: { [Op.iLike]: `%${inscode}%` } } : null;
    StockShare.findAll({where :condition})
    .then(data => {res.send(data);})
    .catch(err => {
        res.status(500).send({
          message:
            err.message || "be fana raft"
        });
    });
};


exports.findByInscode = (req,res) => {
    const reqInscode = req.params.inscode;
    StockShare.findOne({where : {inscode:reqInscode}})
    .then(data => {res.send(data)})
    .catch(err => {
        res.status(500).send({
          message:
            err.message || "be fana raft"
        });
    });
}