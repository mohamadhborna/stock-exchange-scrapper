
module.exports = app =>{

    const stockShares = require("../controllers/stock_share_controller.js");
    var router = require("express").Router();

    router.post("/",stockShares.create);

    router.get("/",stockShares.findAll);

    router.get("/:inscode",stockShares.findByInscode)

    app.use('/api/stockshares', router);
};