const db = require("../models");
const scrape = require('../services/scrape.js')
const StockShare = db.stockShares;

module.exports =()=>{

    StockShare.findAll().then((result) =>{
        for (let i = 0; i < result.length; i++)  {
            inscode = result[i].dataValues.inscode
            let body = scrape(inscode)
            const bodyInscode = body.inscode

            StockShare.update(body,{
                where:{inscode:bodyInscode}
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error updating stock share with inscode =" + inscode
                });
            });
        }
    })
}



