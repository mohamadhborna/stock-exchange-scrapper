const cheerio = require('cheerio');
const axios = require('axios');

module.exports =(inscode) =>{

    let url = 'http://www.tsetmc.com/loader.aspx?ParTree=151311&i='+inscode;
    axios.get(url)
        .then(response =>{  
            const $ = cheerio.load(response);
            let value = $('#MainContent > #TopBox > div.box2.zi1 > div:nth-child(3) > #d10 > div').text()
            let stockShare = {
                inscode : inscode,
                value : value
            };
            return stockShare;
        })
        .catch(error => {console.log(error)})
}