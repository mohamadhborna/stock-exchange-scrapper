
module.exports = (sequelize,Sequelize) => {
    const StockShare = sequelize.define("stockShare",{
        name : { 
            type:Sequelize.STRING
        },
        inscode :{
            type:Sequelize.STRING
        },
        value :{
            type :Sequelize.STRING
        }
    });
    return StockShare;
};