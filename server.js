const express = require("express");
const bodyparser  = require("body-parser");
const cors = require("cors");
const cron = require('node-cron')
const update =require("./app/services/update_value.js")

const app = express();

cron.schedule('*/10 * * * * *',function(){
  update();
})

var corsOption = {
    origin :"http://localhost:4201"
};

app.use(cors(corsOption));

app.use(bodyparser.json());

app.use(bodyparser.urlencoded({extended : true}));

const db = require("./app/models");
db.sequelize.sync();

app.get("/",(req,res) =>{
    res.write("main page")
    res.end()
});

require("./app/routes/stock_share_routes.js")(app);

const PORT = process.env.PORT || 4200;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

